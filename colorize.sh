#!/bin/bash

source_file="regular_vertical_horizontal_waves-1.2_temp.svg"
output_file="regular_vertical_horizontal_waves-1.2_temp_colored.svg"

echo "" > $output_file
while read p; do
  luck=$((1 + RANDOM % 4))
  if [ $luck -eq 1 ]; then
    color="000000"
  elif [ $luck -eq 2 ]; then
    color="ff0000"
  elif [ $luck -eq 3 ]; then
    color="ffff00"
  else
    color="0000ff"
  fi

  echo $p | sed "s#00ff00#$color#g" >> $output_file

done <$source_file
